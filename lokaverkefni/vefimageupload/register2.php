<?php
session_start();
	if(isset($_SESSION['loginusername'])){
		header("location:dashboard.php");
	}
	$errors = [];

	if (isset($_POST['register'])) {
	    $name = trim($_POST['Name']);
	    $email = trim($_POST['email']);
	    $username = trim($_POST['username']);
	    $password = trim($_POST['pwd']);
	    

	    require_once 'db.php';
	    include('users.php');

	    $dbUsers = new Users($conn);
	    $status = $dbUsers->newUser($name,$email,$username,$password);

	    if ($status) {
	        $success = "$username has been registered. You may now log in.";
	    }else{
	        $errors[] = "$username is already in use. Please choose another username.";
	    } 
	}

?>
<!DOCTYPE html>
<html>
<head>
	<title>gallery</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="custom.css">
</head>
<body class="bg">

	<?php
		include('navbar.php');
	?>

	<br>
	<br>
	<h2>Register</h2>
	<?php
		if(isset($success)){
			echo "<p>$success</p>";
		}
		elseif(isset($errors) && !empty($errors)) {
			echo '<ul>';
			foreach ($errors as $error) {
				echo "<li>$error</li>";
			}
			echo '</ul>';
		}
	?>
	<br>
	<form method="post" action="register2.php">
		<p>
			<input type="text" name="Name" placeholder="Full Name" required />
		</p>
		<p>
			<input type="text" name="username" placeholder="User Name" required />
		</p>
		<p>
			<input type="email" name="email" placeholder="Email" required />
		</p>
		<p>
			<input type="password" name="pwd" placeholder="Password" required />
		</p>
		<p>
			<button type="submit" name="register" value="register">Register</button>
		</p>
	</form>


	<img src="css/imgur.png" alt="not imgur">


</body>
</html>