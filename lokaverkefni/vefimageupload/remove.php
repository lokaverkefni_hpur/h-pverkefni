<?php
session_start();
$fileName = $_GET['name'];
$filePath = 'uploads/'.$fileName;

if ( file_exists($fileName) ) {
	unlink($fileName);
	header('Location:dashboard.php');

	$fileName = substr($fileName, 8);

	$errors = [];

	require_once 'db.php';
	require_once 'users.php';

	$dbUsers = new Users($conn);
	$status = $dbUsers->deleteImage($fileName);

	if($status){
		$success = "Success! Your image has been deleted";
	}
	else{
		$errors[] = "Failure! Your image was not been deleted.";
	}
}

?>