<?php
session_start();
	if(!isset($_SESSION['loginusername'])){
		header("location:login.php");
	}

	else{

		$errors = [];


		if (isset($_POST['logout'])){
			//session_start();
			session_destroy();

			header("location: index.php");
		}


		if(isset($_POST['update'])){

			$currname = trim($_POST['currName']);
			$currpass = trim($_POST['currPass']);

			if($_SESSION['loginusername'] == $currname){
				$username = trim($_POST['newName']);
				$useremail = trim($_POST['newEmail']);
				$userpassword = trim($_POST['newPass']);
		
			    require_once 'db.php';
			    require_once 'users.php';
		
			    $dbUsers = new Users($conn);
			    $status = $dbUsers->updateUser($currname,$currpass,$useremail,$username,$userpassword);
			
			    if ($status) {
			    	$_SESSION['loginusername'] = $username;
			        $success = "Success! Your new username is $username.";
			    }else{
			        $errors[] = "Failure! Your username is still $username.";
			    }
			}
			 
		}

		if(isset($_POST['updateImage'])){
			$currname = trim($_POST['currNameImage']);
			$currpass = trim($_POST['currPassImage']);
			
			if(isset($_SESSION['loginusername']) == $currname){
				$currImgName = trim($_POST['currImgName']);
				$newImgName = trim($_POST['newImgName']);

				require_once 'db.php';
				require_once 'users.php';

				$dbUsers = new Users($conn);
				$status = $dbUsers->updateImage($currImgName,$newImgName);

				if ($status) {
			        $success2 = "Your image has been updated.";
			    }else{
			        $errors2[] = "Your image failed to update.";
			    }
			}

		}
	}

	
?>