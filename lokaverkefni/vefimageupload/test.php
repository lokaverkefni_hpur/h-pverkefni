<!DOCTYPE html>
<html>
<head>
	<title>gallery</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="custom.css">
</head>
<body class="bg">

	<br>
	<br>
	<br>
	<br>

	<?php
		include('dashPHP.php');
		include('db.php');
		include('users.php')
	?>

	<br>

	<h2>Update information</h2>
	<br>
	<form class="form-inline" name="update" method="post">
		<div class="form-group">
				<input type="text" name="currName" placeholder="Current Username">
				<input type="password" name="currPass" placeholder="Current Password">
				<input type="text" name="newName" placeholder="New Username">
				<input type="text" name="newEmail" placeholder="New Email">
				<input type="password" name="newPass" placeholder="New Password">
		</div>
		<br>
		<input type="submit" name="update" value="Update">
	</form>

	<br>

	<h2>Update image information</h2>
	<br>
	<form class="form-inline" name="updateImage" method="post">
		<div class="form-group">
				<input type="text" name="currNameImage" placeholder="Current Username">
				<input type="password" name="currPassImage" placeholder="Current Password">
				<input type="text" name="currImgName" placeholder="Current image name">
				<input type="text" name="newImgName" placeholder="New image name">
		</div>
		<br>
		<input type="submit" name="updateImg" value="Update">
	</form>


    <br>

	<form name="logout" method="post">
		<input type="submit" name="logout" value="log out">
	</form>


</body>
</html>