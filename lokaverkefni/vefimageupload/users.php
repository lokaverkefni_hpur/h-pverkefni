<?php
	class Users
	{
		private $connection;
	
		public function __construct($connection_name)
		{
			
			if(!empty($connection_name)){

				$this->connection = $connection_name;
			}
			else{
				throw new Exception("cant connect to database");
			}
		}

		public function uploadToBase($imageName,$imagePath,$userName){
			$sql = "INSERT INTO images (imageName, imagePath, userID) SELECT ?, ?, userID FROM users WHERE userName like ?";

			$statement = $this->connection->prepare($sql);
			$statement->bindParam(1,$imageName);
			$statement->bindParam(2,$imagePath);
			$statement->bindParam(3,$userName);			
			try 
			{
				$statement->execute();
				return true;
			}
			catch(PDOException $e)
			{
				return false;
			}
		}
		
		public function newUser($name,$useremail,$username,$userpassword)
		{
			$sql = "INSERT INTO users (name,useremail,username,userpassword) VALUES (?, ?, ?, ?)";

			$statement = $this->connection->prepare($sql);
			$statement->bindParam(1,$name);
			$statement->bindParam(2,$useremail);
			$statement->bindParam(3,$username);
			$statement->bindParam(4,$userpassword);			
			try 
			{
				$statement->execute();
				
				return true;
			}
			catch(PDOException $e)
			{
				return false;
			}
		}
		
		public function getUser($username)
		{
			$statement = $this->connection->prepare('SELECT userID FROM users WHERE username = ?');
			$statement->bindParam(1,$username);
			
			try 
			{
				$statement->execute();
				
				return true;
			}
			catch(PDOException $e)
			{
				return false;
			}
		}
		
		
		public function userList()
		{
			$statement = $this->connection->prepare('call UserList()');
			
			try 
			{
				$arr = array();
				$statement->execute();
				
				while ($row = $statement->fetch(PDO::FETCH_NUM)) 
				{
					array_push($arr,$row);
				}
				return $arr;
			}
			catch(PDOException $e)
			{
				return array();
			}
		}
	
		public function updateUser($currname,$currpass,$useremail,$username,$userpassword)
		{
			$statement = $this->connection->prepare('UPDATE users SET useremail=?, username=?, userpassword=? WHERE username=? AND userpassword=?');
			$statement->bindParam(1,$useremail);
			$statement->bindParam(2,$username);
			$statement->bindParam(3,$userpassword);
			$statement->bindParam(4,$currname);
			$statement->bindParam(5,$currpass);
			
			try 
			{
				$statement->execute();
				
				return true;
			}
			catch(PDOException $e)
			{
				return false;
			}
		}

		public function updateImage($currImgName,$newImgName)
		{
			$statement = $this->connection->prepare('UPDATE images SET imagename=? WHERE imageName = ?');
			$statement->bindParam(1,$newImgName);
			$statement->bindParam(2,$currImgName);
			
			try 
			{
				$statement->execute();
				
				return true;
			}
			catch(PDOException $e)
			{
				return false;
			}
		}

		public function deleteUser($user_id)
		{
			$statement = $this->connection->prepare('call DeleteUser(?)');
			$statement->bindParam(1,$user_id);
			
			try 
			{
				$statement->execute();
				
				return true;
			}
			catch(PDOException $e)
			{
				return false;
			}
		}

		public function deleteImage($imageName)
		{
			$statement = $this->connection->prepare('DELETE FROM images WHERE imageName = ?');
			$statement->bindParam(1,$imageName);

			try 
			{
				$statement->execute();
				
				return true;
			}
			catch(PDOException $e)
			{
				return false;
			}
		}
		
		public function resetUser($user_id)
		{
			$statement = $this->connection->prepare('call ResetUser(?)');
			$statement->bindParam(1,$user_id);
			
			try 
			{
				$statement->execute();
				
				return true;
			}
			catch(PDOException $e)
			{
				return false;
			}
		}

		public function setAccessLevel($access_level,$user_id,$admin_id)
		{
			$statement = $this->connection->prepare('select SetAccessLevel(?,?,?)');
			$statement->bindParam(1,$access_level);
			$statement->bindParam(2,$user_id);
			$statement->bindParam(3,$admin_id);
			
			try 
			{
				$statement->execute();
				$row = $statement->fetch(PDO::FETCH_NUM);
				
				return $row[0];
			}
			catch(PDOException $e)
			{
				return 0;
			}
		}


		/*public function validateUser($username,$userpassword)
		{
			$sql = "SELECT username,userpassword FROM  users WHERE username = ? AND userpassword = ?";
			$statement = $this->connection->prepare($sql);
			$statement->bindParam(1,$username);
			
			$ret = false;
			
			try 
			{
				$statement->execute();
				$row = $statement->fetch(PDO::FETCH_NUM);
				
				if($row[0] == 1)
					header("location:dashboard.php");
					$ret = true;
			}
			catch(PDOException $e)
			{
				$ret = false;
			}

			
			return $ret;
		}*/
	
		
	}
?>