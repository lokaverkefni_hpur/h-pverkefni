<?php
    /*if(!isset($_SESSION['loginusername'])){
        header("location:login.php");
    }*/
include('db.php');

$i = 1;

if(isset($_POST['submit'])) {
    $tmp = explode(".", $_FILES["fileToUpload"]["name"]);
    $extension = end($tmp);
    $username = $_SESSION['loginusername'];
	$name = $_FILES['fileToUpload']['name'];
	$target_dir = "uploads/";
	$target_file = $target_dir . $username . "." . $extension;
	$uploadOk = 1;
	$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
    $errors = [];


	if(isset($_POST["submit"])) {
	    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    	if($check !== false) {
        	echo "File is an image - " . $check["mime"] . ".";
        	$uploadOk = 1;
    	} 
    	else {
        	echo "You will be redirected, file is not an image.";
            header( "Refresh:2; url=index.php", true, 303);
        	$uploadOk = 0;
    	}
	}

	if (file_exists($target_file)) {
        do{
            $i++;
            $target_file = $target_dir . $username . "(" . $i .  ")." . $extension;
            header("location:index.php");
            $uploadOk =   1;
        }while(file_exists($target_file));
        $name = $username . "(" . $i . ")";
        move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file);
	}

	if ($_FILES["fileToUpload"]["size"] > 5000000) {
    	echo "You will be redirected, your file is too large.";
        header( "Refresh:2; url=index.php", true, 303);
    	$uploadOk = 0;
	}

	if($imageFileType != "jpg" && $imageFileType != "JPG" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
    	echo "You will be redirected, only JPG, JPEG, PNG & GIF files are allowed.";
        header( "Refresh:2; url=index.php", true, 303);
    	$uploadOk = 0;
	}

	if ($uploadOk == 0) {
    	echo "You will be redirected, your file was not uploaded.";
        header( "Refresh:2; url=index.php", true, 303);
	} 
	else {
    	if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        	header('Location:index.php');
    	} 
    	else {
        	echo "You will be redirected, there was an error uploading your file.";
            header( "Refresh:2; url=index.php", true, 303);
    	}
	}
}

?>