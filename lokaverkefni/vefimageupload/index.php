<?php
session_start();
	if(isset($_POST["submit"])){
			$username = $_SESSION['loginusername'];
        	$fileName = $_POST['fileName'];
        	$path = "uploads/";
        	$fileName = $_SESSION['fileName'];

	        require_once 'db.php';
	        require_once 'users.php';

	        $dbUsers = new Users($conn);
	        $status = $dbUsers->uploadToBase($fileName,$path,$username);

	        if ($status) {
	            $success = "Photo has been uploaded";
	        }else{
	            $errors[] = "Photo has not been uploaded";
	        }
		}

?>
<!DOCTYPE html>
<html>
<head>
	<title>gallery</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="custom.css">
	<script type="text/javascript" src="fancybox/lib/jquery-1.10.1.min.js"></script>
	<script type="text/javascript" src="fancybox/source/jquery.fancybox.js?v=2.1.5"></script>
	<link rel="stylesheet" type="text/css" href="fancybox/source/jquery.fancybox.css?v=2.1.5" media="screen" />
</head>

<body class="bg">

	<?php
		include('upload.php');
		$page = 'phpmain';
		include('navbar.php');
		$i++;
	?>

	<br>
	<br>
	<br>
	<?php
		if(isset($_SESSION['loginusername'])){
			echo 'Welcome '.$_SESSION['loginusername'];
		}
		else{
			echo "You need to log in to upload images";
		}
		/*if(isset($_POST["submit"])){
			$username = trim($_SESSION["loginusername"]);
        	$fileName = trim($_POST["fileName"]);
        	$path = "uploads/";

	        require_once 'db.php';
	        require_once 'users.php';

	        $dbUsers = new Users($conn);
	        $status = $dbUsers->uploadToBase($username,$fileName,$path);

	        if ($status) {
	            $success = "Photo has been uploaded";
	        }else{
	            $errors[] = "Photo has not been uploaded";
	        }
		}*/
	?>
	<br>
	<form class="form-inline" role="upload" action="" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <input type="text" name="fileName" id="fileName" placeholder="Image Name" class="form-control">
            <input type="file" name="fileToUpload" <?php if(!isset($_SESSION['loginusername'])) echo 'disabled="disabled"' ?>id="fileToUpload" class="form-control">
        </div>
      	<button type="submit" class="btn btn-default" name="submit">Upload</button>
    </form>    

	<div class="container">
		<div class="row">
			<?php
			$folder = "uploads";
			$results = scandir('uploads');
			$file = $_SESSION['fileName'];
			foreach ($results as $result) {
				if ($result === '.' or $result === '..') continue;

				if (is_file($folder . '/' . $result)) {
					echo '
					<div class="col-xs-6 col-md-3">
						<div class="thumbnail">
						<a href="' . $folder . '/' . $result . '" title="'.$file.'" class="fancyboxEffect" data-fancybox-group = "gallery">
							<img src="'.$folder . '/' . $result. '" alt="..."style="height: 100%; width: 100%; object-fit: contain;">
						</a>
						</div>
					</div>';
				}

			}
			?>
		</div>
	</div>

	<img src="css/imgur.png" alt="not imgur">

	<script>

		$(document).ready(function(){
			$(".fancyboxEffect").fancybox({
				padding: 0,

				openEffect : 'elastic',
				openSpeed : 300,

				closeEffect : 'elastic',
				closeSpeed : 250,

				closeClick : true,

				//maxWidth	: 800,
				//maxHeight	: 600,
				//fitToView	: true,
				//width		: '70%',
				//height		: '70%',
				autoSize	: true,
			});
		});

	</script>


</body>
</html>