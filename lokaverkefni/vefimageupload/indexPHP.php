<?php
session_start();
		if(isset($_POST["submit"])){
			$username = trim($_SESSION["loginusername"]);
        	$fileName = trim($_POST["fileName"]);
        	$path = "uploads/";

	        require_once 'db.php';
	        require_once 'users.php';

	        $dbUsers = new Users($conn);
	        $status = $dbUsers->uploadToBase($username,$fileName,$path);

	        if ($status) {
	            $success = "Photo has been uploaded";
	        }else{
	            $errors[] = "Photo has not been uploaded";
	        }
	        header("location:index.php");
		}
?>