<!DOCTYPE html>
<html>
<head>
</head>
<body>

<?php

function echoActiveClassIfRequestMatches($requestUri)
{
    $current_file_name = basename($_SERVER['REQUEST_URI'], ".php");

    if ($current_file_name == $requestUri){
        echo 'class="active"';
    }
}
?>

    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li <?=echoActiveClassIfRequestMatches("index")?>>
                        <a href="index.php">Home</a>
                    <li <?=echoActiveClassIfRequestMatches("register2")?>>
                        <a href="register2.php">Register</a>
                    </li>
                    <li <?=echoActiveClassIfRequestMatches("login")?>>
                        <a href="login.php">Log in</a>
                    </li>
                    <li <?=echoActiveClassIfRequestMatches("dashboard")?>>
                        <a href="dashboard.php"><?php if(isset($_SESSION["loginusername"])){ echo $_SESSION["loginusername"];};?></a>
                    </li>
                </ul>

            </div>
        </div>
    </nav>


<?php


?>
</body>
</html>