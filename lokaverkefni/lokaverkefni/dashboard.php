<!DOCTYPE html>
<html>
<head>
	<title>gallery</title>
</head>
<body class="bg">

	<br>
	<br>
	<br>
	<br>

	<?php
		include('navbar.php');
		include('dashPHP.php');

		$fileName = $_SESSION['fileName'];
	?>

	<h2><?php echo 'Welcome ' . $_SESSION['loginusername']; ?></h2>

	<br>

	<h3>Update information</h3>
	<br>
	<?php
		if(isset($success)){
			echo "<p>$success</p>";
		}
		elseif(isset($errors) && !empty($errors)) {
			echo '<ul>';
			foreach ($errors as $error) {
				echo "<li>$error</li>";
			}
			echo '</ul>';
		}
	?>
	<form class="form-inline" name="update" method="post">
		<div class="form-group">
				<input type="text" name="currName" placeholder="Current Username">
				<input type="password" name="currPass" placeholder="Current Password">
				<input type="text" name="newName" placeholder="New Username">
				<input type="text" name="newEmail" placeholder="New Email">
				<input type="password" name="newPass" placeholder="New Password">
		</div>
		<br>
		<input type="submit" name="update" value="Update">
	</form>

	<br>

	<!--<h3>Update image information</h3>
	<br>
		<?php
		/*if(isset($success2)){
			echo "<p>$success2</p>";
		}
		elseif(isset($errors2) && !empty($errors2)) {
			echo '<ul>';
			foreach ($errors2 as $error2) {
				echo "<li>$error2</li>";
			}
			echo '</ul>';*/
		}
	?>-->
	<form class="form-inline" name="updateImage" method="post">
		<div class="form-group">
				<input type="text" name="currNameImage" placeholder="Username">
				<input type="password" name="currPassImage" placeholder="Password">
				<input type="text" name="currImgName" placeholder="Current image name">
				<input type="text" name="newImgName" placeholder="New image name">
		</div>
		<br>
		<input type="submit" name="updateImg" value="Update">
	</form>


	<!--<div class="container">
		<div class="row">
		    <?php 
		       	//$folder = "uploads";
		       	//$Username = $_SESSION['loginusername'];
		       	//$results = glob("uploads/$Username*");

		       	//$flag=1;
			   	/*foreach ($results as $result){
			   		echo '<div class="col-md-3">'.PHP_EOL."\t\t";
					echo '<div class="thumbnail' .($flag?' active':''). '">'.PHP_EOL."\t\t";
					echo '<a href="' . $result . '" title="'.$fileName.'" class="fancyboxEffect" data-fancybox-group = "gallery">'.PHP_EOL."\t\t";
					echo '<img src="'.$result.'" alt="">'.PHP_EOL."\t";
					echo '</a>'.PHP_EOL."\t";
					echo '<div class="caption">'.PHP_EOL."\t\t";
					echo '<p><a href="remove.php?name='.$result.'" class="btn btn-danger btn-xs" role="button">Remove</a></p>'.PHP_EOL."\t\t";
					echo '</div>'.PHP_EOL."\t";
					echo '</div>'.PHP_EOL."\t";
					echo '</div>'.PHP_EOL."\t";
					$flag=0;*/
				}
		       ?>
	    </div>
    </div>-->

    <br>

	<form name="logout" method="post">
		<input type="submit" name="logout" value="log out">
	</form>

	<img src="css/imgur.png" alt="not imgur">

	<script>

		$(document).ready(function(){
			$(".fancyboxEffect").fancybox({
				padding: 0,

				openEffect : 'elastic',
				openSpeed : 300,

				closeEffect : 'elastic',
				closeSpeed : 250,

				closeClick : true,

				//maxWidth	: 800,
				//maxHeight	: 600,
				//fitToView	: true,
				//width		: '70%',
				//height		: '70%',
				autoSize	: true,
			});
		});

	</script>


</body>
</html>